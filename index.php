<?php
session_start();
setcookie('pseudo','',time() + 365*24*3600, null, null, false, true); 
setcookie('pays', '', time() + 365*24*3600, null, null, false, true);
?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8"> <!--Code essentiel au référencement, cela permet de traiter les caractères spéciaux-->
  <meta name="description" content="Armurie en ligne">
  <meta name="robots" content="noindex">
	<title>Weapons armory</title>
	<link rel="stylesheet" type="text/css" href="index.css">   
	<link href="https://fonts.googleapis.com/css?family=MedievalSharp&display=swap" rel="stylesheet">
</head>
<body>

    

     <header> <!--header qui sert à contenir la partie nav de la page-->
     	    <h1><img src="images/w.png" class="image">Weapons<br/>armory</h1>
     	    
     	      <nav>
     	      	   <a href="index.php"><p>Accueil</p></a>
     	      	   <a href="boutique.php"><p>Boutique</p></a>
     	      	   <a href="form.php"><p>Contact</p></a>
     	      	   <a href="promo.php"><p>Promotions</p></a>
                 <a href="register.php"><p>Register</p></a>
     	      </nav>
     	        <p class="panier">Panier<img src="icones/Caddie.png"></p>
     </header> <!--fin du header-->


     <div class="banniere"> <!--Cette div contiendra la bannière de la page d'accueil-->
    
      <img src="images/crusader.jpg" class="swaggy">

     </div> <!--fin de la div bannière-->

     <main> <!--le main contiendra la partie description du site web-->
     	  <h2>Weapons armory: votre armurie historique en ligne</h2>
           
     	  <p> ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
     	  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
     	  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
     	  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
     	  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
     	  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
     
     	  <p> ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>


     </main> <!--fin du main-->
     
     <aside> <!--l'aside servira à afficher les nouveautées de la boutique sous forme de carrousel-->
     	   <h1>Articles en ventes:</h3>

          <!--Début du slider-->

 <div class="slideshow-container"> <!--Ceci est la div qui représente l'ensemble du slider, donc tous les éléments du slider seront à l'interieur de cette div-->

<div class="mySlides fade"> <!--Cela correspond tout simplement à la div principale correspondant à la première image du slider, tous les éléments de la première image -->

  <div class="numbertext">1 / 3</div> <!-- Cela correspond au numéro de "pagination" des images du slider, en l'occurence ici, il s'agit de l'image 1 du slider donc on met 1/3 car il y a 3 images au total-->

  <img src="images/heaume.jpg" class="one"> <!-- Insertion de la première image-->
  <div class="text">Heaume de légionnaire romain</div> <!-- Description de l'image-->
</div> <!-- Fin de la div principale-->

<div class="mySlides fade">  <!-- div principale-->
  <div class="numbertext">2 / 3</div> <!--numéro de pagination d'image (2/3)-->
  <img src="images/sabre.jpg" class="one"> <!--insertion de l'image-->
  <div class="text">Sabre orientale</div> <!--description de l'image-->
</div>

<div class="mySlides fade"> <!--div principale-->
  <div class="numbertext">3 / 3</div> <!--numéro de pagination d'image (3/3)-->
  <img src="images/sabre_2.jpg" class="one"> <!--insertion de l'image-->
  <div class="text">Claymore impériale</div> <!--description de l'image-->
</div>

</div>
<br>

<div class="the_dot"> <!-- Div principale qui englobe les indicateurs de page qui sont présentés ici en forme de puces-->
  <span class="dot"></span> 
  <span class="dot"></span> 
  <span class="dot"></span> 
</div> <!-- Fin de la div principale-->


        <!--Fin du slider-->
            
              <a href="boutique.php"><p>Watch our store!!!</p></a>  


     </aside> <!--fin de l'aside-->

     <footer> <!-- le footer contiendra des infos pratiques liées au site -->
     	    <section id="container"> <!-- Les "container" servent à séparer les trois blocs:"Contactez-nous","Informations","Suivez nous sur". -->
     	    	       
     	    	    	<h3>Contactez-nous:</h3> <!-- Premier bloc-->
     	    	    	<p>33 (0)4 56 55 11 83</p>
     	    	    	<p>33 (0)6 44 02 03 74</p>
     	    	    	<p>contact@weaponsarmory.fr</p>
     	    	    
     	    </section>
     	    <section id="container">
     	    	    
     	    	    	<h3>Informations:</h3> <!-- Deuxième bloc-->
     	    	    	<a href="id.php"><p>Qui sommes nous?</p></a>
     	    	    	<a href="info.php"><p>Informations légales</p></a>
     	    	    	<a href="vente.php"><p>Conditions générales de ventes</p></a>
     	    	    
     	    </section>
     	    <section id="container2">
     	    	    	<h3>Suivez nous sur:</h3> <!--Troisième bloc-->
     	    	    	<div class="range">
     	    	        <p><img src="icones/fb.png"></p>
     	    	        <p><img src="icones/twit.png"></p>
     	    	        </div>
     	    </section>
     </footer> <!--fin du footer-->

<script src="index.js"></script>


</body>
</html>