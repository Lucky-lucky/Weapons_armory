<?php
session_start();
setcookie('pseudo','Lucas',time() + 365*24*3600, null, null, false, true); 
setcookie('pays', 'France', time() + 365*24*3600, null, null, false, true);
?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Weapons armory</title>
	<link rel="stylesheet" type="text/css" href="register.css">
     
	<link href="https://fonts.googleapis.com/css?family=MedievalSharp&display=swap" rel="stylesheet">
</head>
<body>
     <header> <!--header qui sert à contenir la partie nav de la page-->
     	    <h1><img src="images/w.png" class="image">Weapons<br/>armory</h1>
     	    
     	      <nav>
     	      	   <a href="index.php"><p>Accueil</p></a>
     	      	   <a href="boutique.php"><p>Boutique</p></a>
     	      	   <a href="form.php"><p>Contact</p></a>
     	      	   <a href="promo.php"><p>Promotions</p></a>
                       <a href="register.php"><p>Register</p></a>
     	      </nav>
     	        <p class="panier">Panier<img src="icones/Caddie.png"></p>
     </header> <!--fin du header-->


     <main>
           <form method="POST" action="">
             <div class="bloc">
              <h1>Register</h1>
               <p>Please fill in this form to create an account.</p>
              <hr>

              <label for="email"><b></b></label>
              <input type="text" placeholder="Enter Email" name="email" id="email" required>

              <label for="psw"><b></b></label>
              <input type="password" placeholder="Enter Password" name="psw" id="psw" required>

              <label for="psw_repeat"><b></b></label>
              <input type="password" placeholder="Repeat Password" name="psw_repeat" id="psw_repeat" required>
             <hr>
              <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>

              <button type="submit" class="registerbtn">Register</button>

              <p>Already have an account? <a href="#">Sign in</a>.</p>

             </div>
  
           </form> 


    <?php

     $bdd = new PDO("mysql:host=localhost;dbname=weapons_armory;port=3308;charset=utf8", "root", "");

     
     
     if (isset($_POST['email']) AND isset($_POST['psw']) AND isset($_POST['psw_repeat']))
     {
       $requete = $bdd->prepare("INSERT INTO register(email, psw, psw_repeat) VALUES( ?, ?, ?)");
       $requete->execute(array($_POST['email'],$_POST['psw'],$_POST['psw_repeat']));
     }

    ?>

     </main>




     <footer> <!-- le footer contiendra des infos pratiques liées au site -->
              <section id="container">
                      
                    <h3>Contactez-nous:</h3>
                    <p>33 (0)4 56 55 11 83</p>
                    <p>33 (0)6 44 02 03 74</p>
                    <p>contact@weaponsarmory.fr</p>
                   
              </section>
              <section id="container">
                   
                    <h3>Informations:</h3>
                    <a href="id.php"><p>Qui sommes nous?</p></a>
                    <a href="info.php"><p>Informations légales</p></a>
                    <a href="vente.php"><p>Conditions générales de ventes</p></a>
                   
              </section>
              <section id="container2">
                    <h3>Suivez nous sur:</h3>
                    <div class="range">
                       <p><img src="icones/fb.png"></p>
                       <p><img src="icones/twit.png"></p>
                       </div>
              </section>
     </footer> <!--fin du footer-->

<script src="index.js"></script>


</body>
</html>