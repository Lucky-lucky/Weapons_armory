<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MySlider</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="test_slider.css">
</head>
<body>
	   <div class="conteneur">
       <div id="yolo" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#yolo" data-slide-to="0" class="active"></li>
    <li data-target="#yolo" data-slide-to="1"></li>
    <li data-target="#yolo" data-slide-to="2"></li>
  </ol>

  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/heaume.jpg" class="d-block w-100" alt="heaume de chevalier">
    </div>
    <div class="carousel-item">
      <img src="images/sabre.jpg" class="d-block w-100" alt="sabre de conquerant">
    </div>
    <div class="carousel-item">
      <img src="images/sabre_2.jpg" class="d-block w-100" alt="claymore imperiale">
    </div>
  </div>
  <a class="carousel-control-prev" href="#yolo" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#yolo" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
      </div>




       <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
       <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
