<?php
  session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Weapons armory</title>
	<link rel="stylesheet" type="text/css" href="form.css">
	<link href="https://fonts.googleapis.com/css?family=MedievalSharp&display=swap" rel="stylesheet">
</head>
<body>
	 <header>
	 	     <h1><img src="images/w.png">Weapons<br/>armory</h1>
     	    
     	      <nav>
     	      	   <a href="index.php"><p>Accueil</p></a>
     	      	   <a href="boutique.php"><p>Boutique</p></a>
     	      	   <a href="form.php"><p>Contact</p></a>
     	      	   <a href="promo.php"><p>Promotions</p></a>
                 <a href="register.php"><p>Register</p></a>
     	      </nav>
     	        <p>Panier<img src="icones/Caddie.png"></p>
	 </header>

     <main>

  <div><img src="images/blasonmirroir.png"></div>

      <fieldset>
     	<legend><img src="icones/Message-rempli.png"></legend>

     	   <form method="POST" action="">

  <div> 
     
   <input pattern="[a-zA-Z-]{0,20}" type="text" name="nom" id="nom" class="champ" placeholder="Votre nom..." /><br /><br /> 

    
   <input pattern="[a-zA-Z]{0,20}" type="text" name="prenom" id="prenom" class="champ" placeholder="votre prénom..." /><br /><br /> 

    
   <input pattern=" [1 - 9]{0,20}" type="number" name="numero" id="numero" class="champ" placeholder="votre numéro..." ></<br /><br /> 
     <br/>
   <textarea name="commentaires" id="commentaires" placeholder="vos commentaires..."></textarea><br/><br/>

    
   <input pattern="[a-zA-Z-[0-9]]+@[a-zA-Z-]+\.[a-zA-Z]{0,20}" type="text" name="mail" id="mail" class="champ" placeholder="votre email..." /><br /><br /> 

   <input type="submit" id="envoi" value="Envoyer" /> 
   <input type="reset" id="rafraichir" value="Rafraîchir" /> 
  </div> 
</form> 


   <?php

     $bdd = new PDO("mysql:host=localhost;dbname=weapons_armory;port=3308;charset=utf8", "root", "");

     
     
     if (isset($_POST['nom']) AND isset($_POST['prenom']) AND isset($_POST['numero']) AND isset($_POST['commentaires']) AND isset($_POST['mail']))
     {
       $requete = $bdd->prepare("INSERT INTO contact(nom, prenom, numero, commentaires, email) VALUES( ?, ?, ?, ?, ?)");
       $requete->execute(array($_POST['nom'],$_POST['prenom'],$_POST['numero'],$_POST['commentaires'],$_POST['mail']));
     }

   ?>









</fieldset>

   <div><img src="images/blason.png"></div>
     </main>

     <footer>
     	     <section id="container">
     	    	       
     	    	    	<h3>Contactez-nous:</h3>
     	    	    	<p>33 (0)4 56 55 11 83</p>
     	    	    	<p>33 (0)6 44 02 03 74</p>
     	    	    	<p>contact@weaponsarmory.fr</p>
     	    	    
     	    </section>
     	    <section id="container">
     	    	    
     	    	    	<h3>Informations:</h3>
     	    	    	<a href="id.php"><p>Qui sommes nous?</p></a>
     	    	    	<a href="info.php"><p>Informations légales</p></a>
     	    	    	<a href="vente.php"><p>Conditions générales de ventes</p></a>
     	    	    
     	    </section>
     	    <section id="container2">
     	    	    	<h3>Suivez nous sur:</h3>
     	    	    	<div class="range">
     	    	        <p><img src="icones/fb.png"></p>
     	    	        <p><img src="icones/twit.png"></p>
     	    	        </div>
     	    </section>
     </footer>
</body>
</html>